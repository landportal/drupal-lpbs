<?php
/**
 *
 */

/**
 * Override the search results page
 */
function lpbs_preprocess_search_result(&$vars){
  if (!empty($vars['result']['node'])) {
    $vars['node'] = node_load($vars['result']['node']->entity_id);
    $vars['content'] = array();
    // Replace node body with the search result matching snippet
    /* if (isset($vars['node'])) { */
    /*   $vars['node']->body['und'][0]['summary'] = */
    /*     $vars['node']->body['und'][0]['safe_summary'] = */
    /*     '<i>' . $vars['result']['snippet'] . '</i>'; */
    /* } */
  }
  //dpm($vars, 'SR');
}
