<?php
/**
 * @file
 * Template to display BS accordion with row classes
 */
//dpm($view, 'view');
?>
<div id="views-bootstrap-accordion-<?php print $id ?>" class="<?php print $classes ?>">
  <?php foreach ($rows as $key => $row):
  //dpm($view->style_plugin->rendered_fields[$key], 'view');
?>
    <div class="panel panel-default<?php if (isset($row_classes[$key])) { print ' ' . implode(' ', $row_classes[$key]); } ?>">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a class="accordion-toggle"
             data-toggle="collapse"
             data-parent="#views-bootstrap-accordion-<?php print $id ?>"
             href="#collapse<?php print $key ?>">
            <?php print $view->style_plugin->rendered_fields[$key][$view->style_options['title_field']];
  //print $titles[$key];
?>
          </a>
        </h4>
      </div>

      <div id="collapse<?php print $key ?>" class="panel-collapse collapse<?php if ($key == '0'): ?> in<?php endif; ?>">
        <div class="panel-body">
          <?php print $row ?>
        </div>
      </div>
    </div>
  <?php endforeach ?>
</div>
