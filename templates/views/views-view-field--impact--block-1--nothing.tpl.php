<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>

<div class="col-md-6 col-lg-4 filtr-item" data-category="<?php print $view->field['field_related_themes']->advanced_render($row) ?>">
  <div class="card border-white shadow p-3 mb-5 bg-white rounded">
<div class="picture rounded img-fluid card-img-top w-100 d-block" 
style="background-image: url('<?php print $view->field['field_image']->advanced_render($row) ?>');"></div>
                        <div class="card-body">
                            <h5 class="card-title m-0"><a href="<?php print $view->field['path']->advanced_render($row) ?>" target="_blank"><?php print $view->field['title']->advanced_render($row) ?></a></h5>
                            <p class="card-text"><?php print $view->field['body']->advanced_render($row) ?></p>
                        </div>
                    </div>
                    </div>







