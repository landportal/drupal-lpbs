<?php
/**
 * @file
 * Stub file for "menu_link" theme hook [pre]process functions.
 */

/**
 * Pre-processes variables for the "menu_link" theme hook.
 *
 * See theme function for list of available variables.
 *
 * @see lpbs_menu_link()
 * @see theme_menu_link()
 *
 * @ingroup theme_preprocess
 */
function lpbs_preprocess_menu_link(array &$variables) {
  $element = &$variables['element'];

  // Determine if the link should be shown as "active" based on the current
  // active trail (set by core/contrib modules).
  // @see https://www.drupal.org/node/2618828
  // @see https://www.drupal.org/node/1896674
  if (in_array('active-trail', _lpbs_get_classes($element)) || ($element['#href'] == '<front>' && drupal_is_front_page())) {
    $element['#attributes']['class'][] = 'active';
  }
  if ($element['#theme'] == 'menu_link__landportal_menu' && $element['#href'] == '<front>') {
    $element['#title'] = '<span class="glyphicon glyphicon-home"></span><span class="sr-only">' . $element['#title'] . '</span>';
    $element['#localized_options']['html'] = true;
  }
  if ($element['#title'] == 'Home') {
    $element['#title'] = '<span class="glyphicon glyphicon-home"></span><span class="sr-only">' . $element['#title'] . '</span>';
    $element['#localized_options']['html'] = true;
    //dpm($element, 'home btn');
  }

  // For main menu Language dropdown link, stay on same page
  if ($element['#original_link']['menu_name'] == 'main-menu' && $element['#title'] == 'Language') {
    if ($element['#href'] != '<droplink>') {
      $element['#href'] = current_path();
    }
  }
}
