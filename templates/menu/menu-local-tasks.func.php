<?php
/**
 * @file
 * Stub file for lpbs_menu_local_tasks().
 */

/**
 * Returns HTML for primary and secondary local tasks.
 *
 * @param array $variables
 *   An associative array containing:
 *     - primary: (optional) An array of local tasks (tabs).
 *     - secondary: (optional) An array of local tasks (tabs).
 *
 * @return string
 *   The constructed HTML.
 *
 * @see theme_menu_local_tasks()
 * @see menu_local_tasks()
 *
 * @ingroup theme_functions
 */
function lpbs_menu_local_tasks(&$variables) {
  $output = '';

  if (!empty($variables['primary'])) {
    // Hide unecessary menus
    $is_front = @$variables['primary'][0]['#active'];
    foreach($variables['primary'] as $id => $link) {
      if (@(isset($link['#active'])
      || $link['#link']['path'] == 'taxonomy/term/%/merge'
      || $link['#link']['path'] == 'user/%/hybridauth'
      || $link['#link']['path'] == 'user/%/shortcuts'
      )) {
        unset($variables['primary'][$id]);
      } else {
        $replace_icon = null;
        if (substr($link['#link']['path'], -4) == 'view') {
          $replace_icon = 'eye-open';
        }
        if (substr($link['#link']['path'], -4) == 'edit') {
          $replace_icon = 'pencil';
        }
        if (substr($link['#link']['path'], -7) == 'display') {
          $replace_icon = 'wrench';
        }
        if (substr($link['#link']['path'], -9) == 'translate') {
          $replace_icon = 'globe';
        }
        if (substr($link['#link']['path'], -5) == 'devel') {
          $replace_icon = 'filter';
        }
        if (substr($link['#link']['path'], -9) == 'revisions') {
          $replace_icon = 'tasks';
        }
        if (substr($link['#link']['path'], -11) == 'linkchecker') {
          $replace_icon = 'screenshot';
        }
        if ($replace_icon) {
          $variables['primary'][$id]['#link']['localized_options']['html'] = TRUE;
          $variables['primary'][$id]['#link']['title'] = '<span class="glyphicon glyphicon-'.$replace_icon.'" title="'
            . $variables['primary'][$id]['#link']['title']
            . '"></span>'
            . '<span class="sr-only">' . $link['#link']['title'] . '</span>';
        }
      }
    }
    //dpm($variables['primary']);
    $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    $variables['primary']['#prefix'] .= '<ul class="tabs--primary nav nav-tabs">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }

  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $variables['secondary']['#prefix'] .= '<ul class="tabs--secondary pagination pagination-sm">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }

  return $output;
}
