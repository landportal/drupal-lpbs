<?php


function lpbs_preprocess_comment(&$variables) {
  // Enhance submitted markup
  if (isset($variables['submitted'])) {
    if ($p = profile2_load_by_user($variables['comment']->uid, 'main')) {
      $pt = $p->view('teaser', 'en');
      // WTF: fix
      $variables['submitted'] = '<div class="profile"><div class="teaser">'
      . render($pt['profile2'][$p->pid]['field_image'])
      . '<div class="profile-info">'
      . $variables['permalink'] . ' Submitted by'
      . '<div class="profile-name">'
      . $variables['author']
      . '</div>'
      . t('!datetime', array('!datetime' => date('d F Y', $variables['comment']->created)))
      . '</div></div></div>';
    }
  }
}
