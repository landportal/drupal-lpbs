<?php
/**
 * @file
 * Stub file for "node" theme hook [pre]process functions.
 */

/**
 * Pre-processes variables for the "node" theme hook.
 *
 * See template for list of available variables.
 *
 * @see node.tpl.php
 *
 * @ingroup theme_preprocess
 */
function lpbs_preprocess_node(&$variables) {
  // Split up translation links, to be able to display them in nodes header (see: node.tpl.php)
  if (isset($variables['content']['links']) && isset($variables['content']['links']['translation'])) {
    $t = $variables['content']['links']['translation'];
    $t['#links'] = array('info' => array('title'=> t('Also in '))) + $t['#links'];
    $variables['content']['translations'] = $t;
    unset($variables['content']['links']['translation']);
  }

  // Hide node title when a title field is present
  //  this is useful for teaser nodes?
  //  for (full) page, this only seem to scope to node tpl, no hack possible at the rendering page tpl level
  if (isset($variables['title_field'])) {
    $variables['title'] = '';
  }

  if ($variables['view_mode'] == 'search_result' || $variables['view_mode'] == 'listing') {
    $e = entity_get_info('node');//.$variables['type']);
    $variables['bundle'] = str_replace('Land', '', $e['bundles'][$variables['type']]['label']);// . '/'.$variables['view_mode'];
    $variables['classes_array'][] = 'node-listing';
  }
  // Enhance submitted markup
  if (isset($variables['submitted']) && $variables['display_submitted']) {
    if ($p = profile2_load_by_user($variables['uid'], 'main')) {
      $pt = $p->view('teaser', 'en');
      //dpm($p, 'profile ' . $up);
      if ($variables['view_mode'] == 'teaser' || $variables['view_mode'] == 'listing') {
        $up = drupal_get_path_alias('user/' . $variables['uid']);
        $variables['submitted'] = '<a href="' . $up . '">' . $p->label . '</a>';
      } else {
        $variables['submitted'] = '<div class="profile"><div class="teaser">'
        . render($pt['profile2'][$p->pid])
        . '</div></div>';
      }
    } else {
      $variables['submitted'] = '<span class="user-name">'
        . t('Submitted by !username', array('!username' => $variables['name']))
        . '</span>'
        ;
    }
    $variables['submitted'] .= '<div class="datetime">'
      . t('!datetime', array('!datetime' => date('d F Y', $variables['created'])))
      . '</div>';
  }
  //$variables['content']['translations']['#printed'] = FALSE;
}
