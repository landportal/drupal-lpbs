<?php
/**
 * @file
 * Stub file for "taxonomy" theme hook [pre]process functions.
 */

/**
 * Pre-processes variables for the "taxonomy" theme hook.
 *
 * See template for list of available variables.
 *
 * @see taxonomy-term.tpl.php
 *
 * @ingroup theme_preprocess
 */
function lpbs_preprocess_taxonomy_term(&$variables) {
  //dpm($variables, 'tax var');
  // Hide node title when a title field is present
	if (isset($variables['name_field'])) {
		$variables['term_name'] = '';
	}

	$variables['classes_array'][] = 'taxonomy-'.$variables['view_mode'];
}
