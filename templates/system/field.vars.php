<?php
/**
 * @file
 * Stub file for "field" theme hook [pre]process functions.
 */

function lpbs_preprocess_field(&$variables) {
  $name = $variables['element']['#field_name'];

  // Image hack, duplicate the image field to have one in the background, for design purposes
  if ($name == 'field_image' && $variables['element']['#view_mode'] == 'full') {
    $node = $variables['element']['#object'];
    if (isset($node->type) && in_array($node->type, array('debate', 'blog_post', 'news', 'event', 'project'))) {
      $variables['items'][] = $variables['items'][0];
    }
  }
}

// From Drupal core modules/field/field.module
function lpbs_field($variables) {
  $output = '';
  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<div class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . ':&nbsp;</div>';
  }
  // Render the items.
  $output .= '<div class="field-items"' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    // LP Hack
    @$item_attr = $variables['item_attributes_array'][$delta];
    if (!$item_attr) {
      $item_attr = array('class' => []);
    }
    $item_attr['class'][] = 'field-item';
    $item_attr['class'][] = ($delta % 2 ? 'odd' : 'even');
    $variables['item_attributes_array'][$delta] = $item_attr;
    $output .= '<div ' . drupal_attributes($variables['item_attributes_array'][$delta]) . '' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
    // Original Drupal code
    // $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    // $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
  }
  $output .= '</div>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';

  return $output;
}
